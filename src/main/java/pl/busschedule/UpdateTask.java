/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import pl.busschedule.database.DBUpdater;
import pl.busschedule.model.Schedule;

public class UpdateTask extends AsyncTask<String, String, UpdateTaskResult> {

    private static final String UNABLE_TO_PARSE_DATA = "Unable to parse data";
    private Context context;
    private OnUpdateTaskCompletedListener listener;

    public UpdateTask(Context context, OnUpdateTaskCompletedListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected UpdateTaskResult doInBackground(String... strings) {
        UpdateTaskResult result = new UpdateTaskResult();
        double timeStamp = 0.0d;
        try {
            Parser parser = new Parser(strings[0]);
            double localTimeStamp = Double.longBitsToDouble(Long.valueOf(strings[1]));
            boolean isFirstTime = Boolean.valueOf(strings[2]);
            if (!isFirstTime) {
                timeStamp = parser.parseTimeStamp();
            }

            if ((timeStamp > localTimeStamp) || isFirstTime) {
                Schedule schedule = parser.parseSchedule();
                DBUpdater dbUpdater = new DBUpdater(context, schedule);
                dbUpdater.update();
            }

        } catch (ParserException e) {
            Log.e(this.getClass().toString(), UNABLE_TO_PARSE_DATA);
            result.setUpdateSuccessful(false);
        }

        result.setTimeStamp(timeStamp);

        return result;
    }

    @Override
    protected void onPostExecute(UpdateTaskResult result) {
        super.onPostExecute(result);
        listener.onTaskCompleted(result);
    }


}
