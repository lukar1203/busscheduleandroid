/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule;

import android.net.http.AndroidHttpClient;
import android.os.Build;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import pl.busschedule.model.Schedule;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class Parser {

    private static final String ALL = "/";
    private static final String ENCODING = "UTF-8";
    private static final int STATUS_CODE_OK = 200;
    private static final String CHECK_UPDATES = "/check-updates";
    private static final String TIME_STAMP = "time_stamp";
    private String url;

    public Parser(String url) {
        this.url = url;
    }

    public double parseTimeStamp() throws ParserException {
        double timeStamp;
        Map<String, Double> map = new Gson().fromJson(loadJson(CHECK_UPDATES),
                new TypeToken<Map<String, Double>>() {
                }.getType());
        timeStamp = map.get(TIME_STAMP);

        return timeStamp;
    }

    private String loadJson(String name) throws ParserException {
        String json;

        try {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.FROYO) {
                json = downloadFroyo(name);
            } else {
                json = download(name);
            }

        } catch (IOException e) {
            throw new ParserException(e);
        }

        return json;
    }

    private String download(String name) throws IOException {
        String json;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url + name).build();
        Response response = client.newCall(request).execute();
        json = response.body().string();
        return json;
    }

    private String downloadFroyo(String name) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url + name);
        AndroidHttpClient.modifyRequestToAcceptGzipResponse(get);
        HttpResponse response = client.execute(get);
        StatusLine statusLine = response.getStatusLine();
        String json = null;
        if (statusLine.getStatusCode() == STATUS_CODE_OK) {
            InputStream content = AndroidHttpClient.getUngzippedContent(response.getEntity());
            json = IOUtils.toString(content, ENCODING);
        }
        return json;
    }

    public Schedule parseSchedule() throws ParserException {
        String json = loadJson(ALL);
        Gson gson = new Gson();
        Schedule schedule = gson.fromJson(json, Schedule.class);

        return schedule;
    }

}
