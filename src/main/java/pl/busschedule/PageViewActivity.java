/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import pl.busschedule.adapters.BusScheduleFragmentAdapter;
import pl.busschedule.adapters.BusStopCursorAdapter;
import pl.busschedule.database.BusStopDao;
import pl.busschedule.database.SQLiteBusDao;
import pl.busschedule.database.SQLiteBusStopDao;
import pl.busschedule.database.tables.BusStopTable;
import pl.busschedule.layouts.Overlay;
import pl.busschedule.layouts.SlidingTabLayout;
import pl.busschedule.model.Bus;

import java.util.ArrayList;
import java.util.List;

public class PageViewActivity extends ActionBarActivity {

    public static final String SELECTED_BUS_STOP = "SELECTED_BUS_STOP";
    public static final String SELECTED_BUS_STOP_NAME = "SELECTED_BUS_STOP_NAME";
    public static final String BUS_NUMBER_PLACEHOLDER_1 = "10";
    public static final String BUS_NUMBER_PLACEHOLDER_2 = "13";
    public static final double FIRST_RUN = -1.0d;
    public static final int BUS_STOP_ID_PLACEHOLDER = 0;
    public static final String IS_FIRST_RUN = "isFirstRun";
    private static final String URL = "http://lukar1203.pythonanywhere.com";
    private static final String TIME_STAMP = "timeStamp";
    private Toolbar toolbar;
    private ViewPager viewPager;
    private SlidingTabLayout slidingTabLayout;
    private BusScheduleFragmentAdapter busScheduleFragmentAdapter;
    private long busStopId;
    private SharedPreferences sharedPrefs;
    private BusStopDao busStopDao;
    private SearchView searchView;
    private ProgressDialog updateProgressDialog;
    private String timeStamp;
    private boolean isFirstRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.page_view_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorAccent);
            }
        });

        loadSharedPreferences();

        if (!isNetworkAvailable()) {
            if (isFirstRun) {
                showRetryUpdateDialog();
            }
        } else {
            runUpdateTask();
        }
        busStopDao = new SQLiteBusStopDao(this);
        populateViewPager(savedInstanceState);
    }

    private void loadSharedPreferences() {
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        timeStamp = String.valueOf(sharedPrefs.getLong(TIME_STAMP, Double.doubleToLongBits(FIRST_RUN)));
        isFirstRun = sharedPrefs.getBoolean(IS_FIRST_RUN, true);
        busStopId = sharedPrefs.getLong(SELECTED_BUS_STOP, BUS_STOP_ID_PLACEHOLDER);
    }

    private void runUpdateTask() {
        showUpdateProgressDialog();
        UpdateTask updateTask = new UpdateTask(this, new OnUpdateTaskCompletedListener() {

            @Override
            public void onTaskCompleted(UpdateTaskResult result) {
                if (result.isUpdateSuccessful()) {
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putLong(TIME_STAMP, Double.doubleToRawLongBits(result.getTimeStamp()));
                    editor.commit();
                } else {
                    Toast.makeText(getBaseContext(), getString(R.string.unable_to_update_schedule), Toast.LENGTH_LONG).show();

                }
                if (isFirstRun) {
                    updateProgressDialog.dismiss();
                    showOverlay();
                }
            }
        });
        updateTask.execute(URL, timeStamp, String.valueOf(isFirstRun));
    }

    private void showRetryUpdateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.no_internet_connection));
        builder.setMessage(getString(R.string.internet_connection_is_required));

        builder.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (isNetworkAvailable()) {
                    runUpdateTask();
                } else {
                    showRetryUpdateDialog();
                }
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void populateViewPager(Bundle savedInstanceState) {
        List<Fragment> fragments;
        if (savedInstanceState != null || busStopId > BUS_STOP_ID_PLACEHOLDER) {
            List<Bus> buses = getBuses(busStopId);
            fragments = getFragments(buses, busStopId);
        } else {
            List<Bus> buses = new ArrayList<Bus>();
            fragments = getFragments(buses, (long) BUS_STOP_ID_PLACEHOLDER);

        }

        busScheduleFragmentAdapter = new BusScheduleFragmentAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(busScheduleFragmentAdapter);
        slidingTabLayout.setViewPager(viewPager);
    }

    private void showUpdateProgressDialog() {
        if (isFirstRun) {
            updateProgressDialog = ProgressDialog.show(this, getString(R.string.downloading_bus_schedule),
                    getString(R.string.please_wait), true);

            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putBoolean(IS_FIRST_RUN, false);
            editor.commit();
        }
    }

    private List<Bus> getBuses(long busStopId) {
        SQLiteBusDao busDao = new SQLiteBusDao(this);

        return busDao.get(busStopId);
    }

    private void showOverlay() {
        Overlay overlay = new Overlay(this, R.style.overlay);
        overlay.show();
    }

    private List<Fragment> getFragments(List<Bus> buses, long busStopId) {
        List<Fragment> fragments = new ArrayList<Fragment>();
        for (Bus bus : buses) {
            fragments.add(BusScheduleFragment.newInstance(busStopId, bus.getBusNumber()));
        }

        if (buses.isEmpty()) {
            fragments.add(BusScheduleFragment.newInstance(BUS_STOP_ID_PLACEHOLDER, BUS_NUMBER_PLACEHOLDER_1));
            fragments.add(BusScheduleFragment.newInstance(BUS_STOP_ID_PLACEHOLDER, BUS_NUMBER_PLACEHOLDER_2));
        }

        return fragments;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong(SELECTED_BUS_STOP, busStopId);

        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem menuItem = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        BusStopCursorAdapter adapter = new BusStopCursorAdapter(getBaseContext(), busStopDao.getCursor(""));
        searchView.setSuggestionsAdapter(adapter);

        MenuItemCompat.expandActionView(menuItem);
        String busStopName = sharedPrefs.getString(SELECTED_BUS_STOP_NAME, "");
        searchView.setQuery(busStopName, false);
        searchView.clearFocus();

        setupQueryListener();
        setupSuggestionListener();

        return true;
    }

    public void setupQueryListener() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                searchView.getSuggestionsAdapter().changeCursor(busStopDao.getCursor(query));
                searchView.getSuggestionsAdapter().notifyDataSetChanged();

                return true;
            }
        });
    }

    public void setupSuggestionListener() {
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int i) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Cursor cursor = (Cursor) searchView.getSuggestionsAdapter().getItem(position);
                int idColumnIndex = cursor.getColumnIndex(BusStopTable._ID);
                busStopId = cursor.getLong(idColumnIndex);

                int nameColumnIndex = cursor.getColumnIndex(BusStopTable.NAME);
                String busStopName = cursor.getString(nameColumnIndex);
                searchView.setQuery(busStopName, false);

                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putLong(SELECTED_BUS_STOP, busStopId);
                editor.putString(SELECTED_BUS_STOP_NAME, busStopName);
                editor.commit();

                List<Bus> buses = getBuses(busStopId);
                List<Fragment> fragments = getFragments(buses, busStopId);
                busScheduleFragmentAdapter = new BusScheduleFragmentAdapter(getSupportFragmentManager(), fragments);
                viewPager.setAdapter(busScheduleFragmentAdapter);
                slidingTabLayout.setViewPager(viewPager);
                searchView.clearFocus();

                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_about: {
                Intent intent = new Intent(this, InfoActivity.class);
                startActivity(intent);

                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }

    }

}