package pl.busschedule.layouts;

import android.app.Dialog;
import android.content.Context;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import pl.busschedule.R;

public class Overlay extends Dialog {

    public Overlay(Context context, int theme) {
        super(context, theme);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.overlay);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setCancelable(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.dismiss();
        return true;
    }
}
