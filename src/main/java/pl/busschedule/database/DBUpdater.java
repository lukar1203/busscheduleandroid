/*
 * Copyright (c) 2015.
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import android.content.Context;
import pl.busschedule.Parser;
import pl.busschedule.model.Schedule;

public class DBUpdater {
    private Context context;
    private Schedule schedule;

    public DBUpdater(Context context, Schedule schedule) {
        this.context = context;
        this.schedule = schedule;
    }

    public void update() {
        updateBusStops();
        updateBuses();
        updateDepartures();
        updateKeys();
        updateRoutes();
    }

    private void updateBusStops() {
        BusStopDao busStopDao = new SQLiteBusStopDao(context);
        busStopDao.delete();
        busStopDao.insert(schedule.getBusStops());
    }

    private void updateBuses() {
        BusDao busDao = new SQLiteBusDao(context);
        busDao.delete();
        busDao.insert(schedule.getBuses());
    }

    private void updateDepartures() {
        DepartureDao departureDao = new SQLiteDepartureDao(context);
        departureDao.delete();
        departureDao.insert(schedule.getDepartures());
    }

    private void updateKeys() {
        KeyDao keyDao = new SQLiteKeyDao(context);
        keyDao.delete();
        keyDao.insert(schedule.getKeys());
    }

    private void updateRoutes() {
        RouteDao routeDao = new SQLiteRouteDao(context);
        routeDao.delete();
        routeDao.insert(schedule.getRoutes());
    }
}