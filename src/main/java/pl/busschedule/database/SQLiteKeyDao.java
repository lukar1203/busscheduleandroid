/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import pl.busschedule.database.tables.KeyTable;
import pl.busschedule.model.InfoContainer;

import java.util.ArrayList;
import java.util.List;

public class SQLiteKeyDao implements KeyDao {

    private DBHelper dbHelper;

    public SQLiteKeyDao(Context context) {
        dbHelper = new DBHelper(context);
    }

    @Override
    public void insert(List<InfoContainer> keys) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        String sql = "INSERT OR REPLACE INTO " + KeyTable.TABLE_NAME + " ( "
                + KeyTable._ID + ", " + KeyTable.SYMBOL
                + ", " + KeyTable.DEFINITION + " ) VALUES ( ?, ?, ? )";

        database.beginTransaction();
        SQLiteStatement statement = database.compileStatement(sql);

        for (InfoContainer key : keys) {
            statement.bindLong(1, key.getId());
            statement.bindString(2, key.getSymbol());
            statement.bindString(3, key.getDefinition());

            statement.execute();
            statement.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }

    @Override
    public List<InfoContainer> get() {
        List<InfoContainer> keys = new ArrayList<InfoContainer>();

        String[] columns = {KeyTable._ID, KeyTable.SYMBOL,
                KeyTable.DEFINITION};

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.query(KeyTable.TABLE_NAME, columns, null, null, null,
                null, KeyTable.SYMBOL + " ASC");

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            InfoContainer key = new InfoContainer();
            key.setId(cursor.getLong(0));
            key.setSymbol(cursor.getString(1));
            key.setDefinition(cursor.getString(2));

            keys.add(key);
            cursor.moveToNext();
        }

        cursor.close();
        database.close();

        return keys;
    }

    @Override
    public void delete() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.execSQL("DELETE FROM " + KeyTable.TABLE_NAME);
        database.close();
    }
}
