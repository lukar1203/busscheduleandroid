/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import pl.busschedule.database.tables.DepartureTable;
import pl.busschedule.model.Departure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SQLiteDepartureDao implements DepartureDao {

    private SQLiteOpenHelper dbHelper;

    public SQLiteDepartureDao(Context context) {
        dbHelper = new DBHelper(context);
    }

    @Override
    public void insert(List<Departure> departures) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        String sql = "INSERT OR REPLACE INTO " + DepartureTable.TABLE_NAME + " ( "
                + DepartureTable._ID + ", " + DepartureTable.HOUR + ", "
                + DepartureTable.MINUTES + ", " + DepartureTable.DAY_TYPE
                + ", " + DepartureTable.BUS_STOP_ID + ", "
                + DepartureTable.BUS_NUMBER + " ) VALUES ( ?, ?, ?, ?, ?, ? )";

        database.beginTransaction();
        SQLiteStatement statement = database.compileStatement(sql);

        for (Departure departure : departures) {
            statement.bindLong(1, departure.getId());
            statement.bindLong(2, departure.getHour());
            statement.bindString(3, departure.getMinutes());
            statement.bindLong(4, departure.getDayType());
            statement.bindLong(5, departure.getBusStopId());
            statement.bindString(6, departure.getBusNumber());

            statement.execute();
            statement.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }

    @Override
    public Map<Long, List<Departure>> get(long busStopId, String busNumber) {
        Map<Long, List<Departure>> departures = new HashMap<Long, List<Departure>>();
        List<Departure> workDayDepartures = new ArrayList<Departure>();
        List<Departure> saturdayDepartures = new ArrayList<Departure>();
        List<Departure> sundayDepartures = new ArrayList<Departure>();
        departures.put(0l, workDayDepartures);
        departures.put(1l, saturdayDepartures);
        departures.put(2l, sundayDepartures);

        String[] columns = {DepartureTable._ID, DepartureTable.HOUR,
                DepartureTable.MINUTES, DepartureTable.DAY_TYPE,
                DepartureTable.BUS_STOP_ID, DepartureTable.BUS_NUMBER};
        String selection = DepartureTable.BUS_STOP_ID + "=? AND "
                + DepartureTable.BUS_NUMBER + "=?";
        String[] selectionArgs = {String.valueOf(busStopId), String.valueOf(busNumber)};

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.query(DepartureTable.TABLE_NAME, columns, selection, selectionArgs, null,
                null, DepartureTable.DAY_TYPE + ", " + DepartureTable.HOUR + " ASC");

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Departure departure = new Departure();
            departure.setId(cursor.getLong(0));
            departure.setHour(cursor.getLong(1));
            departure.setMinutes(cursor.getString(2));
            long dayType = cursor.getLong(3);
            departure.setDayType(dayType);
            departure.setBusStopId(cursor.getLong(4));
            departure.setBusNumber(cursor.getString(5));

            departures.get(dayType).add(departure);
            cursor.moveToNext();
        }

        cursor.close();
        database.close();

        return departures;
    }

    @Override
    public void delete() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.execSQL("DELETE FROM " + DepartureTable.TABLE_NAME);
        database.close();
    }
}
