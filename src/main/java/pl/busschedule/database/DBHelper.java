/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import pl.busschedule.database.tables.BusStopTable;
import pl.busschedule.database.tables.BusTable;
import pl.busschedule.database.tables.DepartureTable;
import pl.busschedule.database.tables.KeyTable;
import pl.busschedule.database.tables.RouteTable;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "bus_schedule.db";
    private static final int DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(BusStopTable.CREATE);
        sqLiteDatabase.execSQL(BusTable.CREATE);
        sqLiteDatabase.execSQL(DepartureTable.CREATE);
        sqLiteDatabase.execSQL(KeyTable.CREATE);
        sqLiteDatabase.execSQL(RouteTable.CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL(BusStopTable.DROP);
        sqLiteDatabase.execSQL(BusTable.DROP);
        sqLiteDatabase.execSQL(DepartureTable.DROP);
        sqLiteDatabase.execSQL(KeyTable.DROP);
        sqLiteDatabase.execSQL(RouteTable.DROP);

        onCreate(sqLiteDatabase);
    }
}
