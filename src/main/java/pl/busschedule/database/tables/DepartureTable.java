/*
 * Copyright (c) 2015.
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database.tables;

import android.provider.BaseColumns;

public class DepartureTable implements BaseColumns {
    public static final String TABLE_NAME = "departure";
    public static final String HOUR = "hour";
    public static final String MINUTES = "minutes";
    public static final String DAY_TYPE = "day_type";
    public static final String BUS_STOP_ID = "bus_stop_id";
    public static final String BUS_NUMBER = "bus_number";

    public static final String CREATE = "CREATE TABLE " + TABLE_NAME + " ( " + _ID
            + " integer primary key autoincrement" + ", " + HOUR + " integer" + ", " + MINUTES
            + " text" + ", " + DAY_TYPE + " integer" + ", " + BUS_STOP_ID
            + " integer" + ", " + BUS_NUMBER + " integer" + ");";
    public static final String DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
