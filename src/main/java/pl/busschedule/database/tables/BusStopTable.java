/*
 * Copyright (c) 2015.
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database.tables;

import android.provider.BaseColumns;

public class BusStopTable implements BaseColumns {
    public static final String TABLE_NAME = "busstop ";
    public static final String NUMBER = "number";
    public static final String NAME = "name";

    public static final String CREATE = "CREATE TABLE " + TABLE_NAME + " ( " + _ID
            + " integer primary key autoincrement" + ", " + NUMBER + " text" + ", " + NAME + " text" + ");";
    public static final String DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
