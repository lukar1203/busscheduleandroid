/*
 * Copyright (c) 2015.
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database.tables;

import android.provider.BaseColumns;

public class RouteTable implements BaseColumns {
    public static final String TABLE_NAME = "route";
    public static final String NUMBER = "number";
    public static final String DESCRIPTION = "description";

    public static final String CREATE = "CREATE TABLE " + TABLE_NAME + " ( " + _ID + " integer primary key autoincrement"
            + ", " + NUMBER + " text" + ", " + DESCRIPTION + " text" + ");";
    public static final String DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
