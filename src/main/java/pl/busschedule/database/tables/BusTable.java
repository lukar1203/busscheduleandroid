/*
 * Copyright (c) 2015.
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database.tables;

import android.provider.BaseColumns;

public class BusTable implements BaseColumns {
    public static final String TABLE_NAME = "bus";
    public static final String NUMBER = "bus_number";
    public static final String BUS_STOP_ID = "bus_stop_id";

    public static final String CREATE = "CREATE TABLE " + TABLE_NAME + " ( " + _ID + " integer primary key autoincrement"
            + ", " + NUMBER + " text" + ", " + BUS_STOP_ID + " integer" + ");";
    public static final String DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
