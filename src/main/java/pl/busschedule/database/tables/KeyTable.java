/*
 * Copyright (c) 2015.
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database.tables;

import android.provider.BaseColumns;

public class KeyTable implements BaseColumns {
    public static final String TABLE_NAME = "key";
    public static final String SYMBOL = "symbol";
    public static final String DEFINITION = "definition";

    public static final String CREATE = "CREATE TABLE " + TABLE_NAME + " ( " + _ID + " integer primary key autoincrement"
            + ", " + SYMBOL + " text" + ", " + DEFINITION + " text" + ");";
    public static final String DROP = "DROP TABLE IF EXISTS " + TABLE_NAME;
}
