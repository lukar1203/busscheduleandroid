/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import pl.busschedule.database.tables.BusTable;
import pl.busschedule.model.Bus;

import java.util.ArrayList;
import java.util.List;

public class SQLiteBusDao implements BusDao {

    public static final String BEACH = "PLAŻA";
    private DBHelper dbHelper;

    public SQLiteBusDao(Context context) {
        dbHelper = new DBHelper(context);
    }

    @Override
    public void insert(List<Bus> buses) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        String sql = "INSERT OR REPLACE INTO " + BusTable.TABLE_NAME + " ( "
                + BusTable._ID + ", " + BusTable.NUMBER
                + ", " + BusTable.BUS_STOP_ID + " ) VALUES ( ?, ?, ? )";

        database.beginTransaction();
        SQLiteStatement statement = database.compileStatement(sql);

        for (Bus bus : buses) {
            statement.bindLong(1, bus.getId());
            statement.bindString(2, bus.getBusNumber());
            statement.bindLong(3, bus.getBusStopId());

            statement.execute();
            statement.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }

    @Override
    public List<Bus> get(long busStopId) {
        List<Bus> buses = new ArrayList<Bus>();

        String[] columns = {BusTable._ID, BusTable.NUMBER,
                BusTable.BUS_STOP_ID};
        String selection = BusTable.BUS_STOP_ID + "=?";
        String[] selectionArgs = {String.valueOf(busStopId)};

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.query(true, BusTable.TABLE_NAME, columns, selection, selectionArgs, BusTable.NUMBER,
                null, "cast( " + BusTable.NUMBER + " as integer)", null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Bus bus = new Bus();
            bus.setId(cursor.getLong(0));
            bus.setBusNumber(cursor.getString(1));
            bus.setBusStopId(cursor.getLong(2));

            buses.add(bus);
            cursor.moveToNext();
        }

        cursor.close();
        database.close();

        if(!buses.isEmpty()) {
            Bus firstBus = buses.get(0);
            if (firstBus.getBusNumber().equals(BEACH)) {
                buses.remove(0);
                buses.add(firstBus);
            }
        }

        return buses;
    }

    @Override
    public void delete() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.execSQL("DELETE FROM " + BusTable.TABLE_NAME);
        database.close();
    }
}
