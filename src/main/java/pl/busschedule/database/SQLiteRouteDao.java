/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import pl.busschedule.database.tables.RouteTable;
import pl.busschedule.model.InfoContainer;

import java.util.ArrayList;
import java.util.List;

public class SQLiteRouteDao implements RouteDao {

    private DBHelper dbHelper;

    public SQLiteRouteDao(Context context) {
        dbHelper = new DBHelper(context);
    }

    @Override
    public void insert(List<InfoContainer> routes) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        String sql = "INSERT OR REPLACE INTO " + RouteTable.TABLE_NAME + " ( "
                + RouteTable._ID + ", " + RouteTable.NUMBER
                + ", " + RouteTable.DESCRIPTION + " ) VALUES ( ?, ?, ? )";

        database.beginTransaction();
        SQLiteStatement statement = database.compileStatement(sql);

        for (InfoContainer route : routes) {
            statement.bindLong(1, route.getId());
            statement.bindString(2, route.getSymbol());
            statement.bindString(3, route.getDefinition());

            statement.execute();
            statement.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }

    @Override
    public List<InfoContainer> get() {
        List<InfoContainer> routes = new ArrayList<InfoContainer>();

        String[] columns = {RouteTable._ID, RouteTable.NUMBER,
                RouteTable.DESCRIPTION};

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.query(RouteTable.TABLE_NAME, columns, null, null, null,
                null, "cast( " + RouteTable.NUMBER + " as integer)");

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            InfoContainer route = new InfoContainer();
            route.setId(cursor.getLong(0));
            route.setSymbol(cursor.getString(1));
            route.setDefinition(cursor.getString(2));

            routes.add(route);
            cursor.moveToNext();
        }

        cursor.close();
        database.close();

        return routes;
    }

    @Override
    public void delete() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.execSQL("DELETE FROM " + RouteTable.TABLE_NAME);
        database.close();
    }
}
