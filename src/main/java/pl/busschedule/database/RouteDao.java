/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import pl.busschedule.model.InfoContainer;

import java.util.List;

public interface RouteDao {

    void insert(List<InfoContainer> routes);

    List<InfoContainer> get();

    void delete();
}
