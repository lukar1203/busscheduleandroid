/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import pl.busschedule.model.Bus;

import java.util.List;

public interface BusDao {

    void insert(List<Bus> buses);

    List<Bus> get(long busStopId);

    void delete();
}
