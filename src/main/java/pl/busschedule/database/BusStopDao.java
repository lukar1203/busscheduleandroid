/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import android.database.Cursor;
import pl.busschedule.model.BusStop;

import java.util.List;

public interface BusStopDao {

    void insert(List<BusStop> busStops);

    Cursor getCursor(String name);

    List<BusStop> get();

    void delete();
}
