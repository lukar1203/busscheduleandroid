/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import pl.busschedule.database.tables.BusStopTable;
import pl.busschedule.model.BusStop;

import java.util.ArrayList;
import java.util.List;

public class SQLiteBusStopDao implements BusStopDao {

    private SQLiteOpenHelper dbHelper;
    private String[] columns;

    public SQLiteBusStopDao(Context context) {
        dbHelper = new DBHelper(context);
    }

    @Override
    public void insert(List<BusStop> busStops) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        String sql = "INSERT OR REPLACE INTO " + BusStopTable.TABLE_NAME + " ( "
                + BusStopTable._ID + ", " + BusStopTable.NUMBER
                + ", " + BusStopTable.NAME + " ) VALUES ( ?, ?, ? )";

        database.beginTransaction();
        SQLiteStatement statement = database.compileStatement(sql);

        for (BusStop busStop : busStops) {
            statement.bindLong(1, busStop.getId());
            statement.bindString(2, busStop.getNumber());
            statement.bindString(3, busStop.getName());

            statement.execute();
            statement.clearBindings();
        }

        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }

    @Override
    public List<BusStop> get() {
        List<BusStop> busStops = new ArrayList<BusStop>();

        columns = new String[]{BusStopTable._ID,
                BusStopTable.NUMBER, BusStopTable.NAME};

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.query(BusStopTable.TABLE_NAME, columns, null, null, null,
                null, BusStopTable.NAME + " ASC");

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            BusStop busStop = new BusStop();
            busStop.setId(cursor.getLong(0));
            busStop.setNumber(cursor.getString(1));
            busStop.setName(cursor.getString(2));

            busStops.add(busStop);
            cursor.moveToNext();
        }

        cursor.close();
        database.close();

        return busStops;
    }

    @Override
    public Cursor getCursor(String name) {
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        String[] selectionArgs = new String[]{"%" + name + "%"};
        Cursor cursor = database.query(BusStopTable.TABLE_NAME, columns, BusStopTable.NAME + " LIKE ?", selectionArgs, null,
                null, "cast( " + BusStopTable.NUMBER + " as integer)");
        cursor.getCount();
        return cursor;

    }

    @Override
    public void delete() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.execSQL("DELETE FROM " + BusStopTable.TABLE_NAME);
        database.close();
    }
}
