/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.database;

import pl.busschedule.model.Departure;

import java.util.List;
import java.util.Map;

public interface DepartureDao {

    void insert(List<Departure> departures);

    Map<Long, List<Departure>> get(long busStopId, String busNumber);

    void delete();
}
