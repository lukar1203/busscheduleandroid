/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import pl.busschedule.adapters.FragmentAdapter;
import pl.busschedule.layouts.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

public class InfoActivity extends ActionBarActivity {
    private Toolbar toolbar;
    private ViewPager viewPager;
    private SlidingTabLayout slidingTabLayout;
    private FragmentAdapter fragmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_view_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorAccent);
            }
        });

        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(new KeyFragment());
        fragments.add(new RouteFragment());
        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(fragmentAdapter);
        slidingTabLayout.setViewPager(viewPager);
    }
}
