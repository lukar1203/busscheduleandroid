/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule;

public class ParserException extends Exception {

    public ParserException() {
        super();
    }

    public ParserException(String detailMessage) {
        super(detailMessage);
    }

    public ParserException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ParserException(Throwable throwable) {
        super(throwable);
    }
}
