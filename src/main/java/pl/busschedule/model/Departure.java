/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.model;

import com.google.gson.annotations.SerializedName;

public class Departure {

    @SerializedName("bus_number")
    private String busNumber;

    @SerializedName("bus_stop_id")
    private long busStopId;

    @SerializedName("day_type")
    private long dayType;

    private long hour;
    private long id;
    private String minutes;

    public long getHour() {
        return hour;
    }

    public void setHour(long hour) {
        this.hour = hour;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getBusNumber() {
        return busNumber;
    }

    public void setBusNumber(String busNumber) {
        this.busNumber = busNumber;
    }

    public long getBusStopId() {
        return busStopId;
    }

    public void setBusStopId(long busStopId) {
        this.busStopId = busStopId;
    }

    public long getDayType() {
        return dayType;
    }

    public void setDayType(long dayType) {
        this.dayType = dayType;
    }
}
