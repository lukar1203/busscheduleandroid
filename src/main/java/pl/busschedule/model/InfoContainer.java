/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.model;

public class InfoContainer {

    private long id;
    private String symbol;
    private String definition;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }
}
