/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Schedule {

    private List<Departure> departures;
    private List<Bus> buses;
    private List<InfoContainer> keys;
    private List<InfoContainer> routes;

    @SerializedName("bus_stops")
    private List<BusStop> busStops;

    public Schedule() {
        departures = new ArrayList<Departure>();
        buses = new ArrayList<Bus>();
        busStops = new ArrayList<BusStop>();
        keys = new ArrayList<InfoContainer>();
        routes = new ArrayList<InfoContainer>();
    }

    public List<Departure> getDepartures() {
        return departures;
    }

    public List<Bus> getBuses() {
        return buses;
    }

    public List<BusStop> getBusStops() {
        return busStops;
    }

    public List<InfoContainer> getKeys() {
        return keys;
    }

    public List<InfoContainer> getRoutes() {
        return routes;
    }
}
