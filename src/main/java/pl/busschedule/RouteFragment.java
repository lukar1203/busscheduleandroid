/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import pl.busschedule.adapters.InfoListViewAdapter;
import pl.busschedule.database.SQLiteRouteDao;
import pl.busschedule.model.InfoContainer;

import java.util.List;

public class RouteFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_fragment, container, false);

        SQLiteRouteDao routeDao = new SQLiteRouteDao(getActivity());
        List<InfoContainer> routes = routeDao.get();
        ListView infoListView = (ListView) view.findViewById(R.id.infoListView);
        ArrayAdapter<InfoContainer> adapter = new InfoListViewAdapter(getActivity(), R.layout.info_list_view_item, routes);
        infoListView.setAdapter(adapter);

        return view;
    }
}
