/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import pl.busschedule.adapters.DeparturesListViewAdapter;
import pl.busschedule.database.SQLiteDepartureDao;
import pl.busschedule.model.Departure;

import java.util.List;
import java.util.Map;

public class BusScheduleFragment extends Fragment {

    public static final String BUS_STOP_ID = "busStopId";
    public static final String BUS_NUMBER = "busNumber";
    private long busStopId;
    private String busNumber;

    public static BusScheduleFragment newInstance(long busStopId, String busNumber) {
        BusScheduleFragment fragment = new BusScheduleFragment();

        Bundle bundle = new Bundle();
        bundle.putLong(BUS_STOP_ID, busStopId);
        bundle.putString(BUS_NUMBER, busNumber);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        busStopId = getArguments().getLong(BUS_STOP_ID);
        busNumber = getArguments().getString(BUS_NUMBER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bus_schedule_fragment, container, false);
        ExpandableListView expandableListView = (ExpandableListView) view.findViewById(R.id.departuresListView);
        SQLiteDepartureDao departureDao = new SQLiteDepartureDao(getActivity());
        Map<Long, List<Departure>> departures = departureDao.get(busStopId, busNumber);

        DeparturesListViewAdapter adapter = new DeparturesListViewAdapter(getActivity(), departures);
        expandableListView.setAdapter(adapter);
        expandableListView.expandGroup(0);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
