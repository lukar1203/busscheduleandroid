/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import pl.busschedule.R;
import pl.busschedule.model.InfoContainer;

import java.util.List;

public class InfoListViewAdapter extends ArrayAdapter<InfoContainer> {

    private Context context;

    public InfoListViewAdapter(Context context, int textViewResourceId, List<InfoContainer> infoContent) {
        super(context, textViewResourceId, infoContent);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.info_list_view_item, parent, false);
        }

        TextView symbol = (TextView) convertView.findViewById(R.id.symbolTextView);
        TextView definition = (TextView) convertView.findViewById(R.id.definitionTextView);

        InfoContainer infoElement = this.getItem(position);
        symbol.setText(infoElement.getSymbol());
        definition.setText(infoElement.getDefinition());

        return convertView;
    }
}
