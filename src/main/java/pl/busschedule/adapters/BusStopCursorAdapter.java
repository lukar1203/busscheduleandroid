package pl.busschedule.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import pl.busschedule.R;
import pl.busschedule.database.tables.BusStopTable;

public class BusStopCursorAdapter extends CursorAdapter {

    private LayoutInflater inflater;

    public BusStopCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, false);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.search_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textView = (TextView) view.findViewById(R.id.searchItemBusStopName);
        String busStopNumber = cursor.getString(cursor.getColumnIndex(BusStopTable.NUMBER));
        String busStopName = cursor.getString(cursor.getColumnIndex(BusStopTable.NAME));
        textView.setText(busStopNumber + ". " + busStopName);
    }
}
