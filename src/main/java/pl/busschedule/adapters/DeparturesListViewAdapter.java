/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import pl.busschedule.R;
import pl.busschedule.model.Departure;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class DeparturesListViewAdapter extends BaseExpandableListAdapter {

    private Context context;
    private String[] dayTypes;
    private Map<Long, List<Departure>> departures;
    private int hour;

    public DeparturesListViewAdapter(Context context, Map<Long, List<Departure>> departures) {
        this.context = context;
        this.departures = departures;

        dayTypes = new String[]{
                context.getString(R.string.weekdays),
                context.getString(R.string.saturday),
                context.getString(R.string.sunday_holidays)
        };
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
    }

    @Override
    public int getGroupCount() {
        return dayTypes.length;
    }

    @Override
    public int getChildrenCount(int i) {
        if (departures.get((long) i) == null) {
            return 0;
        }

        return departures.get((long) i).size();
    }

    @Override
    public Object getGroup(int i) {
        return dayTypes[i];
    }

    @Override
    public Object getChild(int i, int i2) {
        if (i < 0) {
            return null;
        }

        return departures.get((long) i).get(i2);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i2) {
        return i2;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String group = (String) getGroup(i);

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.departures_list_view_group, null);
        }

        TextView groupNameTextView = (TextView) view.findViewById(R.id.departuresGroupNameTextView);
        groupNameTextView.setText(group);

        return view;
    }

    @Override
    public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup) {
        Departure departure = (Departure) getChild(i, i2);

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.departures_list_view_item, null);
        }

        TextView hourTextView = (TextView) view.findViewById(R.id.departureHourTextView);
        TextView minutesTextView = (TextView) view.findViewById(R.id.departureMinutesTextView);
        View separatorLine1 = view.findViewById(R.id.separatorLine1);
        View separatorLine2 = view.findViewById(R.id.separatorLine2);

        if (departure != null) {
            hourTextView.setText(String.valueOf(departure.getHour()));
            minutesTextView.setText(departure.getMinutes());

            if (hour == (int) departure.getHour()) {
                separatorLine1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 8));
                separatorLine2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 8));
            } else {
                separatorLine1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0));
                separatorLine2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0));
            }
        }

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
