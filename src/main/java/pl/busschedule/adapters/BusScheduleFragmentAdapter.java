/*
 * Copyright (c) 2014
 * Łukasz Karpiński <lukar1203@gmail.com>
 * Zbigniew Jankowski <zbigniew.jankowski.88@gmail.com>
 */

package pl.busschedule.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import pl.busschedule.BusScheduleFragment;

import java.util.List;

public class BusScheduleFragmentAdapter extends AbstractFragmentAdapter {

    public BusScheduleFragmentAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm, fragments);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        BusScheduleFragment busScheduleFragment = (BusScheduleFragment) fragments.get(position);
        Bundle bundle = busScheduleFragment.getArguments();
        String busNumber = bundle.getString("busNumber");
        return busNumber;
    }
}
