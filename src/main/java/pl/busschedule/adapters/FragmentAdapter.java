package pl.busschedule.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.List;

public class FragmentAdapter extends AbstractFragmentAdapter {
    private static final String[] PAGE_TITLES = new String[]{"Legenda", "Trasy"};

    public FragmentAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm, fragments);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return PAGE_TITLES[position];
    }

}
